SELECT 
  cid.CODCID AS MUN_CODIGOMUNICIPIO --> 01 C�digo do Munic�pio 
, cid.NOMECID AS MUN_NOME --> 02 Nome do Munic�pio
, ufs.UF AS MUN_ESTADO --> 03 Estado do Munic�pio (UF)
, 'N' AS MUN_PERCFRETE -->04 Percentual Frete CIF
, 'N' AS MUN_PERCFRETEFOB --> 05 Percentual Frete Fob
, 'N' AS MUN_PERCFRETECIFFOB --> 06 Percentual Frete CIF/FOB
FROM sankhya.TSICID as cid
INNER JOIN sankhya.TSIUFS ufs on cid.UF = ufs.CODUF; 
