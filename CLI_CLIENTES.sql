SELECT par.CODPARC AS CLI_CODCLIENTE -- 01 c�digo do cliente  P.CODPARC <> 0 -- Consulta que retorna )
	, par.RAZAOSOCIAL AS CLI_RAZAOSOCIAL -- 02 Raz�o Social P.RAZAOSOCIAL IS NOT NULL Consulta que retorna NULL
	, CASE 
		WHEN par.TIPPESSOA = 'F' 
			 THEN LEFT(SUBSTRING(par.CGC_CPF, 1, 3), 3) + '.' 
				+ LEFT(SUBSTRING(par.CGC_CPF, 4, 6), 3) + '.'
				+ LEFT(SUBSTRING(par.CGC_CPF, 7, 9), 3) + '-'
				+ LEFT(SUBSTRING(par.CGC_CPF, 10, 11), 2)
	ELSE
				  LEFT(SUBSTRING(par.CGC_CPF, 1, 2), 2) + '.' 
				+ LEFT(SUBSTRING(par.CGC_CPF, 3, 5), 3) + '.'
				+ LEFT(SUBSTRING(par.CGC_CPF, 6, 8), 3) + '/'
				+ LEFT(SUBSTRING(par.CGC_CPF, 9, 11), 3) + '-'
				+ LEFT(SUBSTRING(par.CGC_CPF, 12, 13), 2)
	END AS CLI_CGCCPF --03 Cnpj Cliente P.TIPPESSOA IS NOT NULL Consulta que retorna null
	, par.IDENTINSCESTAD AS CLI_INSCRESTADUAL --04 Inscri��o Estadual (Campo Com Null) p.IDENTINSCESTAD IS NOT NULL 
	, e.NOMEEND AS CLI_ENDERECO -- 05 Endere�o ( Consulta que retorna Sem Endere�o) 
	, par.NUMEND AS CLI_NUMERO	-- 06 N�mero do Endere�o (Campo com Null)	
	, par.COMPLEMENTO AS CLI_COMPLEMENTO -- 07 Complemento (Campo Com NULL)
	, bai.NOMEBAI AS CLI_BAIRRO -- 08 BAIRRO
	, par.CODCID AS CLI_CODIGOMUNICIPIO -- 09 C�digo do M�nicipio
	, CASE
		WHEN LEN((RTRIM(REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', '')))) = 12 --> exemplo 551133261912   
			THEN '(' + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 3, 4), 2) + ') '
					 + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 5, 8), 4) + '-'
				     + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 9, 12), 4) 
	
		WHEN LEN((RTRIM(Replace(Replace(TELEFONE, ' ', ''), '-', '')))) = 11 --> exemplo 82996648110
			 THEN '(' + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 1, 2), 2) + ') '
					  + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 3, 7), 5) + '-'
				      + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 8, 11), 4)
		WHEN LEN((RTRIM(REPLACE(REPLACE(TELEFONE, ' ', ''), '-', '')))) = 10 --> exemplo 9236636688
			 THEN '(' + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 1, 2), 2) + ') '
					  + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 3, 6), 4) + '-'
				      + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 7, 10), 4)	
	END 
		AS CLI_TELEFONE --> 10 tELEFONE
	, par.FAX AS CLI_FAX --> 11 faX

	, LEFT(SUBSTRING(par.CEP, 1, 5), 5) + '-' + 
			 LEFT(SUBSTRING(par.CEP, 6, 8), 3) AS CLI_CEP --> 12 CEP
	, CASE
		WHEN CLIENTE = 'S'
			THEN REPLACE('S', CLIENTE, 'A')
		ELSE
			REPLACE('N', CLIENTE, 'I')
		END AS CLI_STATUS --> 13 STATUS DO CLIENTE >>Falta Adicionar a cor na condi��o falsa<<
	, par.NOMEPARC AS CLI_NOMEFANTASIA --> 14 Nome Fantasia
	, CAST(par.DTCAD AS date)  AS CLI_DATACADASTRO --> 15 Data de cadastro
	, par.CODREG AS CLI_CODREG --> 16 C�digo da regi�o
	, 'N' AS CLI_CONDICAOENTREGA  --> 17(Condi��o de entegra) Falta condi��o de entegra
	, 'N' AS CLI_CODRAMO     --> 18 Ramo atividade
	, 'N' AS CLI_CODTABPRECO --> 19 Tabela de Pre�o Padr�o
	, 'N' AS CLI_ULTIMACOMPRA --> 20 Data da �ltima Compra (Campo Vazio)
	, 'N' AS CLI_ULTIMAVISITA --> 21 Data da �ltima visita (Campo NULL)
    , 'N' AS CLI_OBSCREDITO   --> 22 Observa��o de cr�dito e cobran�a (Campo Vazio)
    , par.OBSERVACOES AS CLI_OBSGERAL --> 23 Observa��o Geral
    , par.EMAIL AS CLI_EMAIL               --> 24 E-Mail para envio da c�pia do pedido
	, 'N' AS CLI_PRAZOMAXIMO -- --> 25 Prazo m�ximo permitido (M�DIO) Campo preenchido com zero
	, 'N' AS CLI_CODIGOFORMAPGTO			--> 26 Forma de Pagamento Padr�o CLI_CODIGOFORMAPGTO 
	, 'N' AS CLI_CODIGOFORMAPGTO 		--> 27 Forma de Pagamento Autorizada p/ pedido CLI_FORMASPAGAMENTOS 
	, 'N' AS CLI_DESCFIDELIDADE			--> 28 Desconto Fidelidade (N�o Encontrei)
	, CASE
		WHEN par.BLOQUEAR = 'S'
			THEN REPLACE('S', par.BLOQUEAR, 'SIM')
		ELSE
			REPLACE('N' , par.BLOQUEAR, 'N�O')
		END AS CLI_BLOQUEAR            -->29 Bloquear venda no SFA-Mob?
	, 'N' AS CLI_ALTTABPRECO  --> 30 Flag Permite Alterar Tabela de Pre�o no Pedido
	, 'N' AS CLI_CODIGOCONDPGTO --> 31 Condi��o de Pagamento Padr�o
	, 'N' AS CLI_FINANCEIRO --> 32 Coeficiente Financeiro
	, 'N' AS CLI_PRAZOMINIMOENT --> 33 Prazo m�nimo de entrega (Em dias) -- Campo que retorna NULL na tabela original
	, 'N' AS CLI_PRAZOMAXIMOFT --> 34 Prazo M�ximo de Faturamento (Em Dias) -- Campo Com zero na tabela TGFTPV
	, 'N' AS CLI_OBRIGARMULTPLOEMB --> 35 Obrigar a comprar somente m�ltiplos da Embalagem?
	, 'N' AS CLI_CLIENTEVIP --> 36 CLIENTE VIP
	, 'N' AS CLI_MOTIVOBLOQUEIO --> 37 Motivo bloqueio
	, 'N' AS CLI_TIPOPESSOA --> 38 Tipo pessoa

	, 'N' AS CLI_TRANSPORTADORA --> 39 C�digo transportadora FROM
	
	, 'N' AS CLI_DESCONTO --> 40 Desconto de Contrato 
	, 'N' AS CLI_TRATARLIMITECRED --> 41 Tratar Limite de Cr�dito
	, 'N' AS CLI_TOLERANCIALIMITECRED --> 42 Percentual Exceder Lim.Credito
	, 'N' AS CLI_EMPRESAS --> 43 EMPRESAS PERMITIDAS
	, par.LATITUDE AS CLI_LATITUDE --> 44 LATITUDE
	, par.LONGITUDE AS CLI_LONGITUDE --> 45 LONGITUDE
	, par.TIPPESSOA AS CLI_PESSOA --> 47 PESSOA
	, 'N' AS CLI_ENDERECOENTREGA --> 48 Endere�o da Entrega
	, cpl.NUMENTREGA AS CLI_NUMEROENTREGA --> 49 N�mero da Entrega
	, cpl.COMPLENTREGA AS CLI_COMPLEMENTOENTREGA --> 50 Complemento da entegra
	, 'N' AS CLI_BAIRROENTREGA --> 51 BAIRRO DA ENTREGA
	, cpl.CODCIDENTREGA AS CLI_CODMUNICIPIOENTREGA --> 52 c�digo do munincipio da entrega
	, cpl.CEPENTREGA AS CLI_CEPENTREGA --> 53 CEP de entrega
	, 'N' AS CLI_ENDERECOCOBRANCA --> 54 endere�o de cobran�a
	, 'N' AS CLI_NUMEROCOBRANCA --> 55 N�MERO DE COBRAN�A
	, 'N' AS CLI_COMPLEMENTOCOBRANCA --> 56 COMPLEMENTO DE COBRAN�A
	, 'N' AS CLI_BAIRROCOBRANCA ---> 57 BAIRRO DE COBRAN�A
	, 'N' AS CLI_CODMUNICIPIOCOBRANCA --> 58 C�DIGO DO MUNIC�PIO DE COBRAN�A
	, 'N' AS CLI_CEPCOBRANCA --> 59 CEP COBRAN�A
	, 'N' AS CLI_EMAILSECUNDARIO --> 60 EMAIL SECUND�RIO
	, par.EMAILNFE AS CLI_EMAILNF --> 61 EMAIL NF
	, 'N' AS CLI_CODIGOGRUPOCLIENTE --> 62 CLI_CODIGOGRUPOCLIENTE
	, 'N' AS CLI_PERCFRETE --> 63 Perc. Frete
	, 'N' AS CLI_EMPRESAPADRAO  --> 64 EMPRESA PADR�O
	, 'N' AS CLI_PEDIDOMINIMO --> 65 Valor m�nimo do pedido
	, 'N' AS CLI_PARCELAMINIMA --> 66 PARCELA MINIMA 
	, 'N' AS CLI_REPRESENTANTE --> 67 REPRESENTANTE
	, 'N' AS CLI_ENVIADO --> 68 Enviado
	, 'N' AS CLI_FINANCEIROISENTO --> 69 FINANCEIRO ISENTO
	, 'N' AS CLI_DATAFUNDACAO --> 70 DATA DE FUNDA��O
	, 'N' AS CLI_SUFRAMA  --> 71 SUFRAMA
	, 'N' AS CLI_REFERENCIACOMERCIAL1 --> 72 REFERENCIA COMERCIAL 1         
	, 'N' AS CLI_REFERENCIACOMERCIAL2 --> 73 REFERENCIA COMERCIAL 2
	, 'N' AS CLI_REFERENCIACOMERCIAL3 --> 74 REFERENCIA COMERCIAL 3
	, 'N' AS CLI_TELEFONEREFERENCIACOMERCIAL1 -->75 TELEFONE REFERENCIAL COMERCIAL 1
	, 'N' AS CLI_TELEFONEREFERENCIACOMERCIAL2 -->76  TELEFONE REFERENCIAL COMERCIAL 2
	, 'N' AS CLI_TELEFONEREFERENCIACOMERCIAL3 --> 77 TELEFONE REFERENCIAL COMERCIAL 3
	, 'N' AS CLI_AREACOMERCIAL --> 78 �REA COMERCIAL
	, 'N' AS CLI_CODIGOFAIXAFATURAMENTO --> 79 C�digo da faixa de faturamento
	, 'N' AS CLI_TELEFONEREFERENCIABANCARIA --> 80 TELEFONE REFERENCIA BANCARIA
	, 'N' AS CLI_PREDIOPROPRIO --> 81 Pr�dio pr�prio
	, 'N' AS CLI_FINANCEIRO_APLICA_TABELA -- > 82 Aplica percentual financeiro CLI_FINANCEIRO no preco base da tabela
	, 'N' AS CLI_POSSUIREDE --> 83 POSSUI REDE
	, 'N' AS CLI_NUMEROLOJAS --> 84 N�MERO DE LOJAS
	, 'N' AS CLI_NUMEROCHECKOUTS --> 85 N�mero de checkouts
	, 'N' AS CLI_QTDEFUNCIONARIOS --> 86 QUANTIADE DE FUNCIONARIOS
	, 'N' AS CLI_LIMITECREDBONIF --> 87 Limite de credito para bonifica��es
	, 'N' AS CLI_REGIMEESPECIAL --> 88 REGIME ESPECIAL
	, 'N' AS CLI_VALIDADESIVISA --> 89 Data do vencimento do registro
	, 'N' AS CLI_VALIDADECRF --> 90 Data do vencimento do registro
	, 'N' AS CLI_NUMEROSIVISA  --> 91 Registro SIVISA
	, 'N' AS CLI_NUMEROCRF --> 92 Registro CRF
	, 'N' AS CLI_MARGEMCONTRIBUICAOMINIMA --> 93 Contribui��o m�nima
	, 'N' AS CLI_PAGADORFRETEPADRAO --> 94 Pagador do frete padr�o
	, 'N' AS CLI_TIPOCALCULOFRETE --> 95 Tipo de calculo de frete do cliente. Caso vazio � considerado o Par�metro geral (GUA_PARAMETROS)
	, 'N' AS CLI_REGIMETRIBUTARIO --> 96 Regime tribut�rio do cliente 

FROM sankhya.TGFPAR par

INNER JOIN sankhya.TSIEND e ON par.CODEND = e.CODEND
INNER JOIN sankhya.TSIBAI bai ON par.CODBAI = bai.CODBAI
INNER JOIN sankhya.TGFCPL cpl on cpl.CODPARC = par.CODPARC;
        



