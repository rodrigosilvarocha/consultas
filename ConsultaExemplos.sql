

select cid.NOMECID, cid.uf, uf.UF, uf.DESCRICAO
FROM sankhya.TSICID cid, sankhya.TSIUFS uf
WHERE uf.DESCRICAO like '%SAO PAULO%';

SELECT cid.NOMECID, cid.uf, uf.UF, uf.DESCRICAO--- -> Consulta que retorna a cidade e estado
FROM sankhya.TSICID as cid
JOIN sankhya.TSIUFS uf on cid.UF = uf.CODUF
order by uf.UF; -- Join completa

SELECT cid.CODCID AS 'MUN_CODIGOMUNICIPIO'
    , cid.NOMECID AS 'MUN_NOME'
	, uf.UF AS 'MUN_ESTADO'
FROM sankhya.TSICID as cid
JOIN sankhya.TSIUFS uf on cid.UF = uf.CODUF
WHERE uf.UF <> '0';  --Join eliminando <Sem descri��o> do campo MUN_NOME

-----Clientes Cadastro Principal------
SELECT * from sankhya.TGFPAR; -- Consulta que retorna cliente

SELECT p.CODPARC AS 'CLI_CODCLIENTE' -- 01 c�digo do cliente
	, p.RAZAOSOCIAL AS 'CLI_RAZAOSOCIAL' -- 02 Raz�o Social
	, CASE 
		WHEN p.TIPPESSOA = 'F' 
			 THEN LEFT(SUBSTRING(p.CGC_CPF, 1, 3), 3) + '.' 
				+ LEFT(SUBSTRING(p.CGC_CPF, 4, 6), 3) + '.'
				+ LEFT(SUBSTRING(p.CGC_CPF, 7, 9), 3) + '-'
				+ LEFT(SUBSTRING(p.CGC_CPF, 10, 11), 2)
	ELSE
				  LEFT(SUBSTRING(p.CGC_CPF, 1, 2), 2) + '.' 
				+ LEFT(SUBSTRING(p.CGC_CPF, 3, 5), 3) + '.'
				+ LEFT(SUBSTRING(p.CGC_CPF, 6, 8), 3) + '/'
				+ LEFT(SUBSTRING(p.CGC_CPF, 9, 11), 3) + '-'
				+ LEFT(SUBSTRING(p.CGC_CPF, 12, 13), 2)
	END AS CLI_CGCCPF --03 Cnpj Cliente 


FROM sankhya.TGFPAR p
WHERE P.CODPARC <> 0 AND P.RAZAOSOCIAL IS NOT NULL AND P.TIPPESSOA IS NOT NULL;


---------Formatar cpf-cnpj-------

SELECT * from sankhya.TGFPAR
WHERE TIPPESSOA = 'f';

SELECT TIPPESSOA , 
	CASE 
		WHEN TIPPESSOA = 'F' 
			 THEN LEFT(SUBSTRING(CGC_CPF, 1, 3), 3) + '.' 
				+ LEFT(SUBSTRING(CGC_CPF, 4, 6), 3) + '.'
				+ LEFT(SUBSTRING(CGC_CPF, 7, 9), 3) + '-'
				+ LEFT(SUBSTRING(CGC_CPF, 10, 11), 2)
	ELSE
				LEFT(SUBSTRING(CGC_CPF, 1, 2), 2) + '.' 
				+ LEFT(SUBSTRING(CGC_CPF, 3, 5), 3) + '.'
				+ LEFT(SUBSTRING(CGC_CPF, 6, 8), 3) + '/'
				+ LEFT(SUBSTRING(CGC_CPF, 9, 11), 3) + '-'
				+ LEFT(SUBSTRING(CGC_CPF, 12, 13), 2)
	END AS CPF_CNPJ_FORMATADO
FROM sankhya.TGFPAR;

SELECT SUBSTRING(CGC_CPF, 1, 3) + '.' 
	+ SUBSTRING(CGC_CPF, 4, 6) + '.'
	+ SUBSTRING(CGC_CPF, 7, 9) + '-'
	+ SUBSTRING(CGC_CPF, 10, 11)

FROM sankhya.TGFPAR;

select LEFT('1234', 2);

select RIGHT(replicate(' ', 2) + CGC_CPF, 3)  

from sankhya.TGFPAR;

select x = SUBSTRING(CGC_CPF, 1, 3)
from sankhya.TGFPAR

select NUMEND from sankhya.TGFPAR; --Tabela Cliente
 

-------Join Tabela TGFPAR X Com TSI END-------------

select e.NOMEEND
FROM sankhya.TGFPAR par
JOIN sankhya.TSIEND e ON par.CODEND = e.CODEND ;

-------Join Tabela TSIBAI x TGFPAR------------------

select b.NOMEBAI
FROM sankhya.TGFPAR p
JOIN sankhya.TSIBAI b ON p.CODBAI = b.CODBAI;

------Join Tabela TGFPAR x TSICID-----------------
SELECT distinct par.CODCID

FROM sankhya.TGFPAR par
JOIN sankhya.TSICID cid ON par.CODCID = cid.CODCID;


-----Formartar telefone--------
SELECT '(' + LEFT(SUBSTRING(TELEFONE, 3, 4), 2) + ')' + 
		LEFT(SUBSTRING(TELEFONE, 5, 7), 4) + '-' + 
		LEFT(SUBSTRING(TELEFONE, 8, 10), 4)
FROM sankhya.TGFPAR;

select * from sankhya.TSIUFS; -- C�digo do estado

select * from sankhya.TGFPAR; --Tabela Cliente

select * from sankhya.TSICID; -- C�digo da cidade

select len(par.TELEFONE), par.TELEFONE, uf.UF
FROM sankhya.TGFPAR par
JOIN sankhya.TSICID cid ON cid.CODCID = par.CODCID
JOIN sankhya.TSIUFS uf ON cid.uf = uf.CODUF;

----Formartar CEP

SELECT + LEFT(SUBSTRING(CEP, 1, 5), 5) + '-' + 
			 LEFT(SUBSTRING(CEP, 6, 8), 3) 
FROM sankhya.TGFPAR;


---->Status do Cliente <---

SELECT 
	CASE
		WHEN CLIENTE = 'S'
			THEN REPLACE('S', CLIENTE, 'A')
		ELSE
			REPLACE('N', CLIENTE, 'I')
		END


FROM sankhya.TGFPAR;


----->Jun��o TSIEMP x TGFPAR<-------
SELECT *
FROM sankhya.TGFPAR p
JOIN sankhya.TSIEMP emp ON p.CODCID = emp.CODCID;

SELECT emp.NOMEFANTASIA
FROM sankhya.TGFPAR p
INNER JOIN sankhya.TSIEMP emp ON p.CODCID = emp.CODCID AND p.CODBAI = emp.CODBAI
ORDER BY emp.NOMEFANTASIA;

-->Formartar data de cadastro<--

select DTCAD FROM sankhya.TGFPAR; --Tabela Cliente ativo

SELECT DTCAD AS '@DATE'
FROM sankhya.TGFPAR;

SELECT CAST(DTCAD AS date) 
FROM sankhya.TGFPAR;

DECLARE @date date = DTCAD from sa


------>> Tratamento de telefone <<--------------
select TELEFONE FROM sankhya.TGFPAR; --Tabela Cliente ativo



select CASE
		WHEN len(par.TELEFONE) = 12
			THEN '(' + LEFT(SUBSTRING(par.TELEFONE, 3, 4), 2) + ') '
					 + LEFT(SUBSTRING(par.TELEFONE, 5, 8), 4) + '-'
				     + LEFT(SUBSTRING(par.TELEFONE, 9, 12), 4) 
		
					
				
			END 
		AS TELEFONE,
 len(par.TELEFONE) AS TOTAL_CARACTERES, par.TELEFONE, uf.UF
FROM sankhya.TGFPAR par
JOIN sankhya.TSICID cid ON cid.CODCID = par.CODCID
JOIN sankhya.TSIUFS uf ON cid.uf = uf.CODUF;

SELECT LTRIM(RTRIM(Replace(Replace(telefone, ' ', ''), '-', ''))) AS TELEFONE_FORMATADO, 
LEN(LTRIM(RTRIM(Replace(Replace(telefone, ' ', ''), '-', ''))))
FROM sankhya.TGFPAR;

select (RTRIM(Replace(Replace(telefone, ' ', ''), '-', ''))) AS TELEFONE_FORMATADO , 
LEN((RTRIM(Replace(Replace(telefone, ' ', ''), '-', '')))) AS TOTAL_CARACTERE
from sankhya.TGFPAR
WHERE LEN((RTRIM(Replace(Replace(telefone, ' ', ''), '-', '')))) >=10
AND LEN((RTRIM(Replace(Replace(telefone, ' ', ''), '-', '')))) <= 15;
--(XX) XXXX-XXXX
------------------------------------------------

SELECT
	CASE
		WHEN LEN((RTRIM(REPLACE(REPLACE(TELEFONE, ' ', ''), '-', '')))) = 12 --> exemplo 551133261912   
			THEN '(' + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 3, 4), 2) + ') '
					 + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 5, 8), 4) + '-'
				     + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 9, 12), 4) 
	
		WHEN LEN((RTRIM(Replace(Replace(TELEFONE, ' ', ''), '-', '')))) = 11 --> exemplo 82996648110
			 THEN '(' + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 1, 2), 2) + ') '
					  + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 3, 7), 5) + '-'
				      + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 8, 11), 4)
		WHEN LEN((RTRIM(REPLACE(REPLACE(TELEFONE, ' ', ''), '-', '')))) = 10 --> exemplo 9236636688
			 THEN '(' + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 1, 2), 2) + ') '
					  + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 3, 6), 4) + '-'
				      + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 7, 10), 4)	
		WHEN  TELEFONE LIKE  '011%' AND LEN((RTRIM(REPLACE(REPLACE(TELEFONE, ' ', ''), '-', '')))) = 11 --> exemplo especial para tratar 01172837421
			 THEN '(' + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 2, 3), 2) + ') '
					  + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 4, 7), 5) + '-'
				      + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 8, 11), 4)	
	END 
		AS TELEFONE_TRATADO_FINAL
	, ((REPLACE(REPLACE(telefone, ' ', ''), '-', ''))) AS TELEFONE_TRATADO
	, TELEFONE AS TELEFONE_ORIGINAL
	, LEN(((Replace(Replace(TELEFONE, ' ', ''), '-', '')))) AS TOTAL_CARAC_TEL_TRATADO
FROM sankhya.TGFPAR

WHERE LEN((RTRIM(Replace(Replace(TELEFONE, ' ', ''), '-', '')))) = 11 AND CODPARC = 65;
----------------------------------------------------------------------------
SELECT 
	CASE 
		WHEN  TELEFONE LIKE  '011%' --> exemplo especial para tratar 01172837421
			 THEN '(' + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 2, 3), 2) + ') '
					  + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 4, 7), 5) + '-'
				      + LEFT(SUBSTRING(((REPLACE(REPLACE(TELEFONE, ' ', ''), '-', ''))), 8, 11), 4)
		END
		, TELEFONE AS TELEFONE_ORIGINAL 
FROM sankhya.TGFPAR

WHERE LEN((RTRIM(Replace(Replace(TELEFONE, ' ', ''), '-', '')))) = 11;
---------------------------------------------------------------

select * from sankhya.TGFPAR WHERE TELEFONE = '01172837421' ;
SELECT TELEFONE 
FROM sankhya.TGFPAR 
WHERE TELEFONE LIKE '011%';

SELECT 
	CASE
		WHEN CLIENTE = 'S'
			THEN REPLACE('S', CLIENTE, 'A')
		ELSE
			REPLACE('N', CLIENTE, 'I')
		END


FROM sankhya.TGFPAR;



----------------->Join TGFTPV x TCAMAT x TGFPAR<-----------------------
SELECT * 
FROM sankhya.TGFTPV tpv
INNER JOIN sankhya.TCAMAT mat ON mat.CODTIPVENDA = tpv.CODTIPVENDA
INNER JOIN sankhya.TGFPAR par ON mat.CODPARC = par.CODPARC; 

SELECT 
	CASE
		WHEN BLOQUEAR = 'S'
			THEN REPLACE('S', BLOQUEAR, 'SIM')
		ELSE
			REPLACE('N' , BLOQUEAR, 'N�O')
		END
FROM sankhya.TGFPAR;

---
SELECT BLOQUEAR
FROM sankhya.TGFPAR;


SELECT tpv.PRAZOMEDMAX
FROM sankhya.TGFTPV tpv
INNER JOIN sankhya.TCAMAT mat ON mat.CODTIPVENDA = tpv.CODTIPVENDA
INNER JOIN sankhya.TGFPAR par ON mat.CODPARC = par.CODPARC; 

select tpv.PRAZOMEDMAX 
from sankhya.TGFPAR par
INNER JOIN sankhya.TCAMAT mat on mat.CODPARC = par.CODPARC
INNER JOIN sankhya.TGFTPV tpv on tpv.CODTIPVENDA = mat.CODTIPVENDA;
---------->Join TGFPAR x TGFCAP <<<------------------------

SELECT cab.CODPARCTRANSP
FROM sankhya.TGFPAR par
INNER JOIN sankhya.TGFCAB cab ON par.CODPARC = cab.CODPARC;

select CODPARC
from sankhya.TGFCAB;


-------------->  <-----------------------

select * from sankhya.TDDCAM where NOMECAMPO like '%transp%'; -- localiza a tabela cidade

select * from sankhya.TGFPAR;

--->Campo 39 c�digo Transportadora <--------

select * from sankhya.TDDCAM where NOMECAMPO like '%CODEND%'; -- localiza a tabela transportadora

select * from sankhya.TSIEND;

SELECT par.TRANSPORTADORA AS CLI_TRANSPORTADORA
FROM sankhya.TGFPAR par
INNER JOIN sankhya.TGFCPL cpl on cpl.CODPARC = par.CODPARC;
