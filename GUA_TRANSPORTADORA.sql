SELECT par.CODPARC AS TRA_CODIGO --> 01 C�digo da Transportadora
 , par.NOMEPARC AS TRA_NOME  --> 02 Nome da Transportadora
 , ed.NOMEEND AS TRA_ENDERECO --> 03 Endere�o da Transportadora
 , bai.NOMEBAI AS TRA_BAIRRO --> 04 Bairro da Transportadora
 , cid.NOMECID AS TRA_CIDADE --> 05 Cidade da Transportadora
 , ufs.UF AS TRA_UF  -->06 UF da Transportadora
 , CASE
		WHEN LEN((RTRIM(REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', '')))) = 12 --> exemplo 551133261912   
			THEN '(' + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 3, 4), 2) + ') '
					 + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 5, 8), 4) + '-'
				     + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 9, 12), 4) 
		WHEN LEN((RTRIM(REPLACE(REPLACE(TELEFONE, ' ', ''), '-', '')))) = 10 --> exemplo 9236636688
			 THEN '(' + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 1, 2), 2) + ') '
					  + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 3, 6), 4) + '-'
				      + LEFT(SUBSTRING(((REPLACE(REPLACE(par.TELEFONE, ' ', ''), '-', ''))), 7, 10), 4)	
		
	END 
		AS TRA_TELEFONE --> 07 tELEFONE
, CASE 
	WHEN cid.TIPOFRETE = 'C'
		THEN REPLACE(cid.TIPOFRETE, 'C', 'CIF')
	WHEN cid.TIPOFRETE = 'F'
		THEN REPLACE(cid.TIPOFRETE, 'F', 'FOB')
	WHEN cid.TIPOFRETE = 'S'
		THEN REPLACE(cid.TIPOFRETE, 'S', 'CIF/FOB')
	END AS TRA_TIPOFRETE --> 08 TIPO DE FRETE
FROM sankhya.TGFPAR par
INNER JOIN sankhya.TSIEND ed ON ed.CODEND = par.CODEND
INNER JOIN sankhya.TSIBAI bai ON bai.CODBAI = par.CODBAI
INNER JOIN sankhya.TSICID cid ON cid.CODCID = par.CODCID
INNER JOIN sankhya.TSIUFS ufs ON ufs.CODUF = cid.UF
WHERE par.TRANSPORTADORA = 'S';

SELECT ed.NOMEEND 
FROM sankhya.TSIEND ed
INNER JOIN sankhya.TGFPAR par ON ed.CODEND = par.CODEND;

